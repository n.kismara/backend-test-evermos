<?php
namespace App\Http\Controllers\CheckOut;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repository\CheckOutRepository;
use App\Repository\CartsRepository;
use App\Repository\Products\StockRepository;

class AddCheckOutItem extends Controller{
	protected $checkOut;
	public function __construct(CheckOutRepository $checkOut){
		$this->checkOut = $checkOut;
	}

	public function index(Request $request){
		$this->validate($request,[
			'cart_id' => 'required|integer'
        ]);
		$requiredData = $request->only(['cart_id']);
		$cart = CartsRepository::getCart($requiredData['cart_id']);
		$stock = StockRepository::get($cart->cart_product_id);
		if($stock==0){
			return response()->json(['status'=>'failed','message'=>'Stock not available'], 400);
		}
		$checkOutItem = $this->checkOut->checkOut($requiredData['cart_id']);
		if($checkOutItem){
			CartsRepository::completeCart($requiredData['cart_id']);
			StockRepository::reduce($requiredData['cart_id']);
			return response()->json(['status'=>'success','message'=>'Success to check out item'], 200);
		}
		return response()->json(['status'=>'failed','message'=>'Failed to check out item'], 400);
	}
}