<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api'], function() use ($router) {
	$router->group(['prefix'=>'product/stock','namespace'=>'Products'], function() use ($router) {
     $router->get('get/{id}', 'GetStock@index');
 	});
 	$router->group(['namespace'=>'Carts'],function() use ($router){
 		$router->post('cart/add','AddCart@index');
 	});
 	$router->group(['namespace'=>'CheckOut'],function() use ($router){
 		$router->post('order/checkout','AddCheckOutItem@index');
 	});
});
