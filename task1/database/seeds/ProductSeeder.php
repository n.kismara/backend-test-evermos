<?php

use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([[
        	'id' => 1,
            'product_name' => "Soap",
            'product_description' => "Red Soap",
            'product_stock' => 30,
            'product_seller_id' => 2
        ],[
        	'id' => 2,
            'product_name' => "Shampoo",
            'product_description' => "Shampoo hair protection",
            'product_stock' => 50,
            'product_seller_id' => 2
        ]]);
    }
}
