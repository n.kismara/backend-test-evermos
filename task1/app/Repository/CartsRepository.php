<?php
namespace App\Repository;

use App\Cart;
use App\Repository\Products\StockRepository;

class CartsRepository {
	public static function addCart($data){
		try{
			$checkStock = StockRepository::get($data['product_id']);
			$remainStock = $checkStock - $data['cart_stock'];
			if($checkStock==0 || $remainStock<0) return false;
			$cart = new Cart;
			$cart->cart_description = $data['cart_description'];
			$cart->cart_stock = $data['cart_stock'];
			$cart->cart_status = 'pending';
			$cart->cart_product_id = $data['product_id'];
			$cart->cart_buyer_id = $data['buyer_id'];
			$cart->save();
			return true;
		}catch(\Exception $e){
			return false;
		}
	}

	public static function completeCart($cartId){
		try{
			$cart = Cart::find($cartId);
			$cart->cart_status = 'complete';
			$cart->save();
			return true;
		}catch(\Exception $e){
			return false;
		}
	}

	public static function getCart($cartId){
		$cart = Cart::find($cartId);
		return $cart;
	}
}