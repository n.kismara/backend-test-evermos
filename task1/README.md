###ISSUES###
These are the issues that might be happened
1. When customer checked out the item, the backend didn't send back the updated stock to the frontend. So customers didn't know the real remaining stock and keep clicking to add quantity of ordered item.
2. Because it was large event, a lot of customer expected to order same item at the same time and it could caused the negative quantity stock. If backend did not check the stock before save the checked out items. 

###SOLUTION###
1. When check out items, backend will response with updated stock
2. always check the stock before update to database.

###How To Run###
1. copy .env.example to .env and fill the configuration
2. run composer install
3. run command "php artisan migrate"
4. run command "php artisan db:seed"
5. serve with this command php -S localhost:8000 -t public
6. access localhost:8000/key and copy the result to .env

###Endpoint###
1. GET base_url/api/product/stock
2. POST base_url/api/cart/add
3. POST base_url/api/order/checkout