<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([[
        	'id' => 1,
            'user_username' => "User 1",
            'user_email' => "user1@gmail.com",
            'user_password' => Hash::make('password1'),
            'user_is_seller' => '0',
        ],[
        	'id' => 2,
            'user_username' => "User 2",
            'user_email' => "user2@gmail.com",
            'user_password' => Hash::make('password2'),
            'user_is_seller' => '1',
        ]]);
    }
}
