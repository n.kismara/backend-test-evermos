<?php
namespace App\Http\Controllers\Carts;

use App\Http\Controllers\Controller;
use App\Repository\CartsRepository;
use App\Repository\Products\StockRepository;
use Illuminate\Http\Request;

class AddCart extends Controller{
	protected $cart;

	public function __construct(CartsRepository $cart){
		$this->cart = $cart;
	}

	public function index(Request $request){
		$this->validate($request,[
			'cart_description' => 'required|string',
            'cart_stock' => 'required|integer',
            'buyer_id' => 'required|integer',
            'product_id' => 'required|integer'
        ]);
		$requiredData = $request->only(['cart_description', 'cart_stock', 'buyer_id', 'product_id']);
		$updateStock = $this->cart->addCart($requiredData);
		$stock = StockRepository::get($requiredData['product_id'],$requiredData['cart_stock']);
		$response = ['status'=>'failed','message'=>'Failed to add product to Cart','result'=>['stock'=>$stock]];
		$statusCode = 400;
		if($updateStock){
			$response = ['status'=>'success','message'=>'Success to add product to Cart','result'=>['stock'=>$stock]];
			$statusCode = 201;
		}
		return response()->json($response, $statusCode);
	}
}