<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCartTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cart', function (Blueprint $table) {
            $table->id();
            $table->string('cart_description', 100);
            $table->enum('cart_status', ['pending', 'delete','complete']);
            $table->unsignedBigInteger('cart_product_id');
            $table->unsignedBigInteger('cart_buyer_id');
            $table->foreign('cart_product_id')->references('id')->on('products');
            $table->foreign('cart_buyer_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cart');
    }
}
