<?php
namespace App\Repository\Products;

use App\Products;
use App\Cart;
class StockRepository {
	public static function get($productId){
		try{
			$stock = 0;
			$productsStock = Products::find($productId);
			$cartStock = Cart::where('cart_product_id',$productId)->where('cart_status','complete')->sum('cart_stock');
			if($productsStock){
				$stock = $productsStock->product_stock-$cartStock;
			}

			return $stock;
		}catch(\Exeption $e){
			return 0;
		}
	}

	public static function reduce($cartId){
		try{
			$cart = Cart::find($cartId);
			$productsStock = Products::find($cart->cart_product_id);
			$productsStock->product_stock = $productsStock->product_stock-$cart->cart_stock;
			$productsStock->save();
			return true;
		}catch(\Exeption $e){
			return false;
		}
	}
}