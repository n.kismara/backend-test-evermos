<?php
namespace App\Repository;

use App\Order;
use App\Repository\Products\StockRepository;

class CheckOutRepository {
	public static function checkOut($cartId){
		try{
			$checkOrder = Order::where('order_cart_id',$cartId)->count();
			if($checkOrder>0) return false;
			$order = new Order;
			$order->order_cart_id = $cartId;
			$order->order_payment_status = 'unpaid';
			$order->save();
			return true;
		}catch(\Exception $e){
			return false;
		}
	}
}