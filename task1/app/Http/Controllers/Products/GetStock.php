<?php
namespace App\Http\Controllers\Products;

use App\Http\Controllers\Controller;
use App\Repository\Products\StockRepository;

class GetStock extends Controller{

	protected $stock;

	public function __construct(StockRepository $stock){
		$this->stock = $stock;
	}
	/**
	* @param int
	* @return json
	*/
	public function index($id){
		return response()->json(['message' => 'Success','result'=>['stock'=>$this->stock->get($id)]], 200);
	}
}